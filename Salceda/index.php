<?php
 $age = 21; 
echo "<br>If-Elseif-Else<br>";
 if($age > 21) {
 echo "Your 21 years old<br>";
 }
elseif($age == 21) { 
echo "You're 21 years old<br>"; 
}else { 
       echo "You're below 21 years old<br>";
 }
 echo "<br>While Loop<br>"; 
$i = 0;
while($i < 5) { 
echo "Only Juliena<br>";
 $i++; 
}
 echo "<br>Do-While Loop<br>";
 $i = 0; 
do { 
echo "Only Juliena<br>"; 
$i++; 
}while($i < 5); 
echo "<br>For Loop<br>";
 for($i = 0; $i < 5; $i++) { 
echo "Only Juliena<br>"; 
} 
echo "<br>ForEach Loop<br>"; 
$num = array(10, 5, 20, 40, 60);
 foreach($num as $n) {
 echo $n . "<br>"; 
} 
?>
